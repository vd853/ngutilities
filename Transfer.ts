export class FileTransfer{
    //slices one file and returns a bunch of chunks.
    //Note, this method causes browser UI to lock, but works for large file transfers
    //Your first is here like this "const file = event.path[0].files[0]" after user selects the file on (change)
    static Slicer(file: any, callback:(data:any)=>void){
        const slices = []
        console.log(file.name)
        const totalSize = file.size;
        const delta = 50000000; //10per 10000000 //number of bytes per packet
        const segmentsDelta = Math.floor(totalSize/delta); //number of segments that needs to be sent for every delta number of bytes
        const remainingDelta = totalSize - segmentsDelta * delta //the last number of bytes for the last packet
        console.log('total: ' + totalSize);
        console.log('segmentsDelta: ', segmentsDelta)
        console.log('remainingDelta ', remainingDelta)
        for(let i = 0; i < segmentsDelta+1; i++){
            const peice = file.slice(i*delta, delta + i*delta);
            callback(peice)
        }
    }

    //event is usually a (change) from file input. Only use for one file. Limit is 50mb
    static FileEventToBase64(event: any, callback:(data:any)=>void){
        const file = event.path[0].files[0]
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            callback(reader.result)
        };
    }
}