// Easy way of defining forms formcontrolname with validator
// It has addEmail, addNumber, addText, addPhone
// This will generate an HTML template in the console.log
// usage
// constructor(private builder: FormBuilder) {
//   this.loginForm = new FormConstructor(builder);
//   this.loginForm.addText('username', undefined, undefined);
//   this.loginForm.build();
// }

// import { ReactiveFormsModule } from '@angular/forms';

// <mat-form-field>
// <input formControlName = "search" [(ngModel)]="model.search" matInput placeholder="Search">
// </mat-form-field>
// <span *ngIf="form.formGroup.controls.search.errors?.required">required error</span><br>
// 
// check form valid like this this.form.formGroup.valid
import { FormBuilder, Validators, FormGroup, FormControl} from '@angular/forms';
import { FormControlName } from '@angular/forms/src/directives/reactive_directives/form_control_name';
export class FormConstructor{
  formGroup: FormGroup; //remember to always call build, otherwise error will occur
  private addedCount = 0;
  private group = [];
  private names:Array<string> = [];
  private HTMLParts = '';
  private builder = new FormBuilder;
  private type = 0;
  constructor(){

  }
  getName(index: number):string{
    return this.names[index];
  }
  isValid(formControlName: any):boolean{
    if(this.formGroup.controls[formControlName]){ //if exist
      if(formControlName === 'description'){
        this.formGroup.controls[formControlName].markAsDirty()
        console.log(this.formGroup.controls[formControlName])
      }
      return !this.formGroup.controls[formControlName].valid;
    }
  }
  addText(formControlName: string, minLength: number = null, maxLength: number=null, required = true){
    this.group[formControlName] = ['', this.addV(required, null, null, minLength, maxLength, null)];
  }
  addEmail(formControlName: string, required = true){
    this.group[formControlName] = ['', this.addV(required, null, null, null, null, true)];
  }
  addNumber(formControlName: string, min: number = null, max: number = null, required = true){
    this.group[formControlName] = ['', this.addV(required, min, max, null, null, null)];
  }

  //You need to implement phone validator sometime
  addPhone(formControlName: string, required = true){
    this.group[formControlName] = ['', this.addV(required, null, null, null, null, null)];
  }

  addGeneric(formControlName: string, required = true){
    this.group[formControlName] = ['', this.addV(required, null, null, null, null, null)];
  }
  private generate(){
    console.log('<form [formGroup] = "form.formGroup">' + '\n' +
                '<mat-form-field>' + '\n' +
                  '<input [(ngModel)]="model.component" formControlName="component" placeholder="Component">' + '\n' + 
                '</mat-form-field>' + '\n' + 
                '</form>'
    )
  }
  private addV(
    required: boolean, 
    min: number, 
    max: number, 
    minLength: number, 
    maxLength: number,
    email: boolean
    ){
    const validators = [];
    if(required) validators.push(Validators.required)
    if(min) validators.push(Validators.min(min))
    if(max) validators.push(Validators.max(max))
    if(minLength) validators.push(Validators.minLength(minLength))
    if(maxLength) validators.push(Validators.maxLength(maxLength))
    if(email) validators.push(Validators.email)
    return Validators.compose(validators)
  }
  private getType(type: number): string{
    switch (type) {
      case 0:
        return 'text';
      case 1:
        return 'email';
      case 2:
        return 'number';
      case 3:
        return 'phone';
    }
  }
  build(){
    this.formGroup = this.builder.group(this.group);
    this.generate()
  }
}
